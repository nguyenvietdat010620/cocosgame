export enum enumkeyBoard {
  "LEFT" = "left",
  "RIGHT" = "right",
  "UP" = "up",
  "DOWN" = "down",
  "SPACE" = "space"
}
