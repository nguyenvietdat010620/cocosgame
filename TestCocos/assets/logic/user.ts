import { _decorator, Component, input,Input,EventKeyboard, UITransform, KeyCode } from 'cc';
import { enumkeyBoard } from '../constant/user.constant';
const { ccclass, property } = _decorator;

@ccclass('user')
export class user extends Component {

    @property speed: number = 10
    @property numberOfBoomb = 2
    
    isMove = false
    movewhere: enumkeyBoard = null

    onkeyPress = (event: EventKeyboard)=>{
        switch (event.keyCode) {
            case KeyCode.ARROW_LEFT:
                this.movewhere = enumkeyBoard.LEFT
                this.isMove = true
                break;
            case KeyCode.ARROW_RIGHT:
                this.movewhere = enumkeyBoard.RIGHT
                this.isMove = true
                break;
            case KeyCode.ARROW_UP:
                this.movewhere = enumkeyBoard.UP
                this.isMove = true
                break;
            case KeyCode.ARROW_DOWN:
                this.movewhere = enumkeyBoard.DOWN
                this.isMove = true
                break;
            case KeyCode.SPACE:
                console.log("space")
            default:
                break;
        }
    }

    onkeyUp = (event: EventKeyboard)=>{
        switch (event.keyCode) {
            case KeyCode.ARROW_LEFT:
                this.isMove = false
                break;
            case KeyCode.ARROW_RIGHT:
                this.isMove = false
                break;
            case KeyCode.ARROW_UP:
                this.isMove = false
                break;
            case KeyCode.ARROW_DOWN:
                this.isMove = false
                break;
            default:
                break;
        }
    }

    moveAction = (deltaTime: number)=>{
        switch (this.movewhere) {
            case enumkeyBoard.RIGHT:
                this.node.setPosition(this.node.getPosition().x += this.speed * deltaTime, this.node.position.y)
                break;
            case enumkeyBoard.LEFT:
                this.node.setPosition(this.node.getPosition().x -= this.speed * deltaTime, this.node.position.y)
                break;
            case enumkeyBoard.UP:
                this.node.setPosition(this.node.position.x, this.node.getPosition().y += this.speed * deltaTime)
                break;
            case enumkeyBoard.DOWN:
                this.node.setPosition(this.node.position.x, this.node.getPosition().y -= this.speed * deltaTime)
                break;
            default:
                break;
        }
    }

    onLoad(){
       input.on(Input.EventType.KEY_DOWN, this.onkeyPress, this)
       input.on(Input.EventType.KEY_UP, this.onkeyUp, this)
    }

    start() {
        
    }

    update(deltaTime: number) {
        // console.log(deltaTime)
        if(this.isMove){
            this.moveAction(deltaTime)
        }
    }
}

